﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {

	public Transform destination;
	public AudioClip teleportSound;

	void OnTriggerEnter(Collider collider){

		AudioSource.PlayClipAtPoint(teleportSound, destination.position);

		if(collider.gameObject.tag == Tags.pickUpObject || collider.gameObject.tag == Tags.explosiveObject){

			Vector3 pos = new Vector3(destination.position.x, destination.position.y, destination.position.z);

			Instantiate(collider.gameObject, pos, Quaternion.identity);
			Destroy(collider.gameObject);
		}
		else if(collider.gameObject.tag == Tags.player){

            Vector3 pos = new Vector3(destination.position.x, destination.position.y, destination.position.z);

			collider.gameObject.transform.position = pos;
		}
	}
}
