﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour
{
    public Font myFont;
    public int fontSize = 30;
    public string button1 = "Start Game";
    public string button2 = "Quit";
    public int xOffset = 100;
    public int yOffset = 100;
    public int xOffset2 = 100;
    public int yOffset2 = 200;
    public AudioClip clip;
    private AudioSource audioSource;
    private SplineController spline;
    private bool buttonOne;
    private bool buttonTwo;
    private bool showGUI = true;

    void start()
    {
        GUI.skin.font = myFont; 
        spline = GameObject.FindGameObjectWithTag(Tags.mainCamera).GetComponent<SplineController>();
    }
    
    void OnGUI()
    {

        displayGUI(showGUI);

        //Act on button click
        if (buttonOne)
        {
                        
            if (showGUI)
            {
                spline = GameObject.FindGameObjectWithTag(Tags.mainCamera).GetComponent<SplineController>();
                spline.FollowSpline();  
                            
            }
            showGUI = false;
            if (spline.stillActive())
            {

                Application.LoadLevel(1);
            }

        }
        if (buttonTwo)
        {
            Application.Quit();
        }

    }

    void displayGUI(bool show)
    {
        if (show)
        {
            GUI.backgroundColor = Color.clear;
        
            // Create style for a button
            GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
            myButtonStyle.fontSize = fontSize;
            myButtonStyle.font = myFont;
        
            // Set color for selected and unselected buttons
            myButtonStyle.normal.textColor = Color.white;
            myButtonStyle.hover.textColor = Color.black;
            myButtonStyle.alignment = TextAnchor.MiddleLeft;
        
            // use style in button
            buttonOne = GUI.Button(new Rect(xOffset, yOffset, 1000, 50), button1, myButtonStyle);
            buttonTwo = GUI.Button(new Rect(xOffset2, yOffset2, 1000, 50), button2, myButtonStyle);
        }
    }
}



