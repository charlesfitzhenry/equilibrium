﻿using UnityEngine;
using System.Collections;

public class PuzzleHint : MonoBehaviour
{

    public string inputKey;
    public string puzzleHint;
    public float displayTime = 20f;

    public string getPuzzleHint()
    {
        return puzzleHint;
    }

    public string getInputKey()
    {
        return inputKey;
    }

    public float getDisplayTime()
    {
        return displayTime;
    }
}
