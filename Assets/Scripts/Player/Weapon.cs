﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{
    //Components
    public Transform spawn;
    private LineRenderer tracer;
    
    public enum GunType
    {
        Semi,
        Burst,
        Auto }
    ;

    public GunType gunType;
    public float rpm;
    public int maxShots = 10;
    public int shots = 10;
    public int rounds = 3;
    private float secondsBetweenShots;
    private float nextPossibleShootTime;
    private AudioSource audioSource;
    public AudioClip gunShot;
	private Camera camera;

	public AudioClip enemyHitSound;
	public AudioClip[] bulletCollisionSounds;

    void Start()
    {
        secondsBetweenShots = 60 / rpm;
        //If there is a line renderer attached, set to tracer
        if (GetComponent<LineRenderer>())
        {
            tracer = GetComponent<LineRenderer>();
        }

        audioSource = GetComponent<AudioSource>();
		camera = transform.parent.GetComponent<Camera>();
    }
            
    public void Shoot()
    {
        if (canShoot())
        {
            Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.0f));
            // For info on what ray hits.
            RaycastHit hit; 
            float shotDist = 500;
            
            if (Physics.Raycast(ray, out hit, shotDist))
            {
                if (hit.collider.GetType() == typeof(CapsuleCollider) && hit.collider.gameObject.tag == Tags.enemy)
                {
					AudioSource.PlayClipAtPoint(enemyHitSound, hit.collider.gameObject.transform.position, 100);

                    EnemyHealth enemyHealth = hit.collider.gameObject.GetComponent<EnemyHealth>();
                    enemyHealth.DecrementHealth(20);

                } else if (hit.collider.gameObject.tag == Tags.explosiveObject)
                {
					ExplosiveObject explosiveObject = hit.collider.gameObject.GetComponent<ExplosiveObject>();
                    explosiveObject.DestroyObject();
                }
				else{

					int rand = Random.Range(0, bulletCollisionSounds.Length);
					AudioSource.PlayClipAtPoint(bulletCollisionSounds[rand], hit.collider.gameObject.transform.position, 100);

				}

                //Distance between origin of ray and object hit.
                shotDist = hit.distance;    
            }

            // Test shot.
            Debug.DrawRay(ray.origin, ray.direction * shotDist, Color.red, 1);
            
            nextPossibleShootTime = Time.time + secondsBetweenShots;            

			AudioSource.PlayClipAtPoint(gunShot, transform.position, 0.75f);

            decrementAmmo();
            damage(hit);

            if (tracer)
            {
                StartCoroutine("RenderTracer", ray.direction * shotDist);                   
            }
        }
    }

    void damage(RaycastHit h)
    {
        //check if object hit contains a damageScript
        //If so, destruct object or reduce health depending on the context
        //eg. hit.
        //GameObject g = h.collider.gameObject;
        //Destroy (g);
    }
    
    public void shootContinuous()
    {
        
        if (gunType == GunType.Auto)
        {
            Shoot();
        }
    }

    private void decrementAmmo()
    {
        if (shots == 0)
        {
            if (rounds != 0)
            {
                rounds--;
                shots += 10;
            } 
        } else
        {
            shots--;
        }
    }

    public bool haveAmmo()
    {
        if (rounds == 0 && shots == 0)
        {
            return false;
        } else
        {
            return true;
        }
    }

    private void addAmmo(int r)
    {
        rounds += r;
    }

    public bool canReload(){

        if(shots == maxShots){
            return false;
        }

        return true;
    }

    public void reload(){

        if(rounds > 0){
            rounds--;
            shots = maxShots;
        }
    }
    
    public bool canShoot()
    {
        bool canShoot = true;
        
        if (Time.time < nextPossibleShootTime || !haveAmmo())
        {
            canShoot = false;
        }
        
        return canShoot;
    }
    
    IEnumerator RenderTracer(Vector3 hitPoint)
    {
        
        tracer.enabled = true;
        tracer.SetPosition(0, spawn.position);  //start
        tracer.SetPosition(1, spawn.position + hitPoint);   //end
        //Wait for some time
        yield return null;  //Single frame
        
        tracer.enabled = false;     
    }
}
