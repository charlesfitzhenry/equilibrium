﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
    //public AudioClip deathClip;                          // How much time from the player dying to the player respawning.
    public bool playerDead;                              // A bool to show if the player is dead or not.
    
    private int health = 100;                            // How much health the player has left.
    private FirstPersonController firstPersonController; // Reference to the FirstPersonController script.
    private SceneFadeInOut sceneFadeInOut;               // Reference to the SceneFadeInOut script.
    private LastPlayerSighting lastPlayerSighting;       // Reference to the LastPlayerSighting script.
    private Weapon weapon;                               // Reference to the Weapon script.
    private float constant = 1.0f;                       // Used to calculate gamma.
    private float maximumWaitTime = 10.0f;               // Maximum time to wait while in the process of regenerating health.
    private float timeUntilRegen = 10.0f;                // Time to wait after player takes damage until health regeneration process begins. 
    private float healthNormalized;                      // Health converted to a value between 0 and 1 (inclusive).            
    private float gamma;                                 // Used to calculate timeToWait. 
    private float timeToWait;                            // Time to wait during the process of regenerating health.
    
    
    void Awake()
    {
        // Setting up the references.
        firstPersonController = GetComponent<FirstPersonController>();
        sceneFadeInOut = GameObject.FindGameObjectWithTag(Tags.fader).GetComponent<SceneFadeInOut>();
        lastPlayerSighting = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<LastPlayerSighting>();
		weapon = transform.FindChild("Camera/Weapon").GetComponent<Weapon>();
    }
    
    void FixedUpdate()
    {
        timeUntilRegen -= Time.deltaTime;
        
        if((health < 100) && (timeUntilRegen <= 0))
        {
            RegenerateHealth();
        }
        // If health is equal to 0...
        else if (health == 0)
        {   
            // ... and if the player is not yet dead...
            if (!playerDead)
                // ... call the PlayerDying function.
                PlayerDying();
            else
            {
                // Otherwise, if the player is dead, call the PlayerDead and SpawnPlayer functions.
                PlayerDead();
				Application.LoadLevel(Application.loadedLevel);
                //SpawnPlayer();
            }
        }
    }
    
    private void PlayerDying()
    {
        // The player is now dead.
        playerDead = true;
        
        // Play the dying sound effect at the player's location.
        //AudioSource.PlayClipAtPoint(deathClip, transform.position);
    }
    
    private void PlayerDead()
    {
        // Disable the player movement.
        firstPersonController.enabled = false;
        // Disable the player's weapon.
        //weapon.weaponEnabled = false;
        
        // Reset the player sighting for the enemies.
        lastPlayerSighting.position = lastPlayerSighting.resetPosition;
    }
    
    private void SpawnPlayer()
    {
        // ... spawn the player.
        sceneFadeInOut.SpawnPlayer();
    }
    
    public int Health
    {
        get
        {
            return health;
        }
        set
        {
            health = value;
        }
    }
    
    public void DecrementHealth(int value)
    {
        if(health - value < 0)
        {
            health = 0;
        }
        else
        {
            health -= value;
        }
        timeUntilRegen = 10.0f;
    }
    
    private void IncrementHealth(int value)
    {
        if(health + value > 100)
        {
            health = 100;
        }
        else
        {
            healthNormalized = health / 100.0f;
            gamma = constant / healthNormalized;
            timeToWait = maximumWaitTime / gamma;
            health += value;
        }
    }
    
    private void RegenerateHealth()
    {
        timeToWait -= Time.deltaTime;
        if(timeToWait <= 0)
        {
            IncrementHealth(5);
        }
    }
}
