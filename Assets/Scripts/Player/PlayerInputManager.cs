﻿using UnityEngine;
using System.Collections;

public class PlayerInputManager : MonoBehaviour
{
    private Weapon weapon;
    private Telekinesis telekineses;
    private PlayerInteractionMode playerInteraction;
	private Animator weaponAnimator;
	private AudioSource audioSource;
	private PlayerToggleSwitch switchToggle;

	//Weapon Sounds
	public AudioClip reloadSound;
	public AudioClip switchToWeaponSound;
	public AudioClip noAmmoSound;

	private bool telekinesis;
	
	// Use this for initialization
    void Start()
    {
		telekineses = transform.FindChild("Camera/Weapon").GetComponent<Telekinesis>();
		weapon = transform.FindChild("Camera/Weapon").GetComponent<Weapon>();
        playerInteraction = GetComponent<PlayerInteractionMode>();
		weaponAnimator = transform.FindChild("Camera/Weapon").GetComponent<Animator> ();
		audioSource = GetComponent<AudioSource>();
		switchToggle = GetComponent<PlayerToggleSwitch>();

		//Set start mode to telekinesis
		playerInteraction.setMode (PlayerInteractionMode.InteractionMode.Telekinesis);
		weaponAnimator.SetBool("Telekinesis", true);
		telekinesis = true;
    }
    
    void Update()
    {

		//Switch mode between telekinesis
		if (Input.GetButtonDown ("SwitchMode")) {
			playerInteraction.switchMode();

			AudioSource.PlayClipAtPoint(switchToWeaponSound, weapon.gameObject.transform.position, 1.0f);
			
			if (playerInteraction.getMode() == PlayerInteractionMode.InteractionMode.Shooting){
				Debug.Log("P1 Shooting");
                telekineses.dropObject();   //Drop object if in shooting mode
				weaponAnimator.SetBool("Telekinesis", false);
			}
			else{
				Debug.Log ("P1 Telekinesis");
				weaponAnimator.SetBool("Telekinesis", true);
			}
		}
		
		//Only allow shooting if in shooting mode
		if (playerInteraction.getMode() == PlayerInteractionMode.InteractionMode.Shooting) {

			if (Input.GetButtonDown ("Shoot")) {
				weapon.Shoot ();
				weaponAnimator.SetTrigger("Shooting");
				
			}

			if(Input.GetButtonDown ("Reload") && weapon.canReload()){
				
				weaponAnimator.SetTrigger("Reload");
				//Play reload sound
				AudioSource.PlayClipAtPoint(reloadSound, weapon.gameObject.transform.position, 1.0f);

				//Add clip
				weapon.reload();
			}
	
			//Dry fire no ammo
			if(Input.GetButtonDown ("Shoot") && !weapon.haveAmmo()){

				AudioSource.PlayClipAtPoint(noAmmoSound, weapon.gameObject.transform.position, 1.0f);
			}
		}
		
	}
	
	void FixedUpdate()
	{
		//Only allow telekinesis if in telekinesis mode
		if (playerInteraction.getMode() == PlayerInteractionMode.InteractionMode.Telekinesis) {

			telekineses.telekinesisControler (Input.GetButton ("PickUp"), Input.GetButtonDown ("Shoot"), Input.GetButtonDown ("PickUp"), Input.GetButtonUp ("SwitchMode"));
		}

		switchToggle.toggleSwitch(Input.GetButtonDown("PickUp"));
    }
}
