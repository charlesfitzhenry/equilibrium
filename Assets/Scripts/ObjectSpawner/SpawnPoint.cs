﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {

	public bool hasObject;

	void OnTriggerEnter(Collider otherObject){
		
		hasObject = true;
		
	}

	void OnTriggerExit(Collider otherObject){

		hasObject = false;

	}
}
