﻿using UnityEngine;
using System.Collections;

public class CustomObjectSpawner : MonoBehaviour {
	
	private Transform spawnPos;
	public GameObject objectToSpawn;
	private AudioSource audioSource;
	public AudioClip toggleSound;

	private SpawnPoint spawnPoint;
	private bool canSpawn;

	void Start () {
	
		spawnPos = transform.FindChild("SpawnLocation");
		audioSource = GetComponent<AudioSource>();
		spawnPoint = transform.FindChild("SpawnLocation").gameObject.GetComponent<SpawnPoint>();
	}

	void HitByRaycast(GameObject source){

		GameObject[] allPlatforms = GameObject.FindGameObjectsWithTag("SpawnedPlatform");
		
		if(allPlatforms.Length >= 1){
			
			foreach (GameObject platform in allPlatforms){
				Destroy(platform);
			}
		}

		//if(!spawnPoint.hasObject){

			Instantiate(objectToSpawn, spawnPos.position, spawnPos.rotation);

			//Play toggle sound
			audioSource.clip = toggleSound;
			audioSource.Play();
		//}

	}
}
