﻿using UnityEngine;
using System.Collections;

public class MuzzleFlash : MonoBehaviour
{

    private float FadeSpeedAmount = 0.075f;             // amount of alpha to be deducted each frame
    private Color MuzzleFlashColor = new Color(1, 1, 1, 0.0f);
    private Transform MuzzleFlashTransform = null;
    private Light LightSource = null;
    private float LightIntensity = 0.0f;
    private Renderer Renderer = null;
    private Material Material = null;
    public float FadeSpeed = 20.0f;

    void Awake()
    {
        
        MuzzleFlashTransform = transform;
        
        // if a light is present in the prefab we will cache and use it
        LightSource = light;
        if (LightSource != null)
        {
            LightIntensity = LightSource.intensity;
            LightSource.intensity = 0.0f;
        }
        
        Renderer = renderer;
        if (Renderer != null)
        {
            Material = renderer.material;
            if (Material != null)
            {
                // the muzzleflash is meant to use the 'Particles/Additive'
                // (unity default) shader which has the 'TintColor' property
                MuzzleFlashColor = Material.GetColor("_TintColor");
                MuzzleFlashColor.a = 0.0f;
            }
        }
        
    }
    
    void Update()
    {
        // always fade out muzzleflash if it is visible
        if (MuzzleFlashColor.a > 0.0f)
        {
            MuzzleFlashColor.a -= FadeSpeedAmount * (Time.deltaTime * 60.0f);
            if (LightSource != null)
                LightSource.intensity = LightIntensity * (MuzzleFlashColor.a * 2.0f);   // sync light intensity to muzzleflash alpha
        }
        
        if (Material != null)
            Material.SetColor("_TintColor", MuzzleFlashColor);
        
        if (MuzzleFlashColor.a < 0.01f)
        {
            Renderer.enabled = false;
            if (LightSource != null)
                LightSource.enabled = false;
        }
        
    }

    public void Shoot()
    {
        MuzzleFlashColor.a = 0.5f;  // the default alpha value for the 'Particles/Additive' shader is 0.5

        MuzzleFlashTransform.Rotate(0, 0, Random.Range(0, 360));    // rotate randomly 360 degrees around z
        Renderer.enabled = true;
        
        if (LightSource != null)
        {
            LightSource.enabled = true;
            LightSource.intensity = LightIntensity;
        }
    }

}
